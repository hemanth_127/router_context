import React from 'react'
import { useParams } from 'react-router-dom'

const UserDetails = () => {
  const {userId} = useParams()   // to get the location of the  URL (userID) 
  // const userId = params.userId

  return <div>Details about User {userId}</div>
}

export default UserDetails
