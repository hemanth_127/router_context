import React from 'react'
import { NavLink } from 'react-router-dom'

const Navbar = () => {
  const NavLinkStyles = ({ isActive }) => {
    return {
      fontWeight: isActive ? 'bold' : 'normal',
      textDecoration: isActive ? 'none' : 'underline'
    }
  }
  return (
    <nav>
      <NavLink style={NavLinkStyles} to='/'>
        home
      </NavLink>
      <NavLink style={NavLinkStyles} to='About/'>
        about
      </NavLink>

      <NavLink style={NavLinkStyles} to='products/'>
        products
      </NavLink>
      <NavLink style={NavLinkStyles} to='users/'>
        users
      </NavLink>
    </nav>
  )
}

export default Navbar
