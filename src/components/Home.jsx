import React from 'react'
import { useNavigate } from 'react-router-dom'

const Home = () => {
  const navigate = useNavigate()
  return (
    <>
      <div>Home Page</div>
      <button
        onClick={() => {
          navigate(
            'Order-summary'
            //    ,{ replace: true } // it will go allthe way back to the history starting page
          )
        }}
      >
        Place Order
      </button>
    </>
  )
}

export default Home
