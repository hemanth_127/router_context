import React from 'react'
import { Link, Outlet } from 'react-router-dom'

const Products = () => {
  return (
    <>
      <div>
        <input type='search' placeholder='Search Products' />
      </div>
      <nav>
          {/* // these are relative links */}
        <Link to='featured'>Featured</Link>
        <Link to='new'>new</Link>
      </nav>
      <Outlet />
    </>
  )
}

export default Products
