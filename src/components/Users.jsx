import React from 'react'
import { Outlet, useSearchParams } from 'react-router-dom'

const Users = () => {
  const [searchParams, setSearchParams] = useSearchParams() //searchparams used to filter the url
  const showActiveUsers = searchParams.get('filter') === 'active'
  return (
    <>
      <div>
        <h2>user 1</h2>
        <h2>user 2</h2>
        <h2>user 3</h2>
      </div>
      <Outlet />
      <div>
        <button onClick={() => setSearchParams({ filter: 'active' })}>
          Active Users
        </button>
        <button onClick={() => setSearchParams({})}>Reset Filter</button>

        {showActiveUsers ? (
          <p>Showing active users only.</p>
        ) : (
          <h2>showing all users</h2>
        )}
      </div>
    </>
  )
}

export default Users
