// App.js
import React from 'react';
import ThemeProviderComponent from './Context/ThemeProviderComponent';
import ThemedComponent from './Context/ThemedComponent';

function App() {
  return (
    <ThemeProviderComponent>
      <div>
        <h1>Theme Example</h1>
        <ThemedComponent />
      </div>
    </ThemeProviderComponent>
  );
}

export default App;
