import React from 'react'
import ReactDOM from 'react-dom/client'
import ContextApp from './ContextApp.jsx'
import { BrowserRouter } from 'react-router-dom'
import App from './App.jsx'

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <BrowserRouter>
    {/* <ContextApp/> */}
      <App/>
    </BrowserRouter>
  </React.StrictMode>
)
