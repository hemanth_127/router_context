// ThemeProviderComponent.js
import React, { useState } from 'react';
import { ThemeProvider } from './ThemeContext';

function ThemeProviderComponent({ children }) {
  const [theme, setTheme] = useState('light');

  const toggleTheme = () => {
    setTheme(prevTheme => (prevTheme === 'light' ? 'dark' : 'light'));
  };

  return (
    <ThemeProvider value={{ theme, toggleTheme }}>
      {children}
    </ThemeProvider>
  );
}

export default ThemeProviderComponent;
