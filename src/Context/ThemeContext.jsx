// ThemeContext.js
import React from 'react';

const ThemeContext = React.createContext();

// Export a Provider and a Consumer
export const ThemeProvider = ThemeContext.Provider;
export const ThemeConsumer = ThemeContext.Consumer;

export default ThemeContext;
