// ThemedComponent.js
import React from 'react';
import { ThemeConsumer } from './ThemeContext';

function ThemedComponent() {
  return (
    <ThemeConsumer>
      {({ theme, toggleTheme }) => (
        <div
          style={{
            backgroundColor: theme === 'light' ? '#ffffff' : '#333333',
            color: theme === 'light' ? '#333333' : '#ffffff',
            padding: '20px',
            textAlign: 'center',
          }}
        >
          <h1>Themed Component</h1>
          <p>Current Theme: {theme}</p>
          <button onClick={toggleTheme}>Toggle Theme</button>
        </div>
      )}
    </ThemeConsumer>
  );
}

export default ThemedComponent;
